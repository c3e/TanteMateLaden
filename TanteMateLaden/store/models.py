from datetime import timedelta
from django.db import models
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.utils.functional import cached_property
from django.dispatch import receiver
from django.conf import settings
from django.urls import reverse
from django.utils import timezone
from rest_framework.authtoken.models import Token
from decimal import Decimal, InvalidOperation
from .mqtt import publish_stock


class Account(models.Model):
    """
    Extends the default user model with specific foo
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to="uploads/avatars/", blank=True, null=True)
    free_access = models.BooleanField("Zugriff ohne Authentifizierung", default=False)
    no_logs = models.BooleanField("Keine Logs", default=False)
    anonon_print = models.BooleanField("Anonym drucken", default=False)
    truncate_logs = models.PositiveIntegerField("Nur Logs über die letzten n Tage (0=alle behalten)", default=0)
    pin = models.CharField("Optional Pin", max_length=64, blank=True, null=True)
    balance = models.DecimalField("Guthaben in Euro", max_digits=5, decimal_places=2, default=0)
    creation_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s account (balance: %s)" % (self.user.username, self.balance)

    @cached_property
    def token(self):
        return Token.objects.get_or_create(user=self.user)[0]

    def truncateLogs(self):
        """
        check if log truncating is active and do so if required.
        """
        if self.truncate_logs > 0:
            pivot_date = timezone.now() - timedelta(days=self.truncate_logs)
            logs = self.user.transactionlog_set.filter(date__lt=pivot_date)
            return logs.delete()
        return False

    def addFunds(self, amount=0, ip=None, user_doing=None, comment="Aufladung"):
        """
        add funds to the account, negative values are allowed.
        """

        if isinstance(amount, (int, float, str)):
            try:
                amount = Decimal(amount)
            except InvalidOperation:
                raise TypeError
            if (not self.no_logs) or (user_doing is None) or (self.user != user_doing):
                log = TransactionLog.objects.create(user=self.user, balance_change=amount, ip=ip,
                                                    user_doing=user_doing, comment=comment)
                self.truncateLogs()
            self.balance += amount
            self.save()
            return True
        else:
            raise TypeError

    def buyItem(self, item, amount=1, ip=None, user_doing=None, comment=None):
        """
        Buys an item(Object or id) w/ the account, changes the balance accordingly and creates a log entry if enabled
        returns the new Balance if successful.
        """
        if isinstance(item, int):
            # get Item instance if id was given
            item = Item.objects.get(id=item)
        if isinstance(item, Item):
            if not isinstance(user_doing, User):
                user_doing = None  # Anonymous User
            if (not self.no_logs) or (user_doing is None) or (self.user != user_doing):
                log = TransactionLog.objects.create(user=self.user, balance_change=item.price * -amount, ip=ip,
                                                    user_doing=user_doing, comment=comment,
                                                    item=item, item_amount=amount)
                self.truncateLogs()
            if item.stock > 0:
                item.stock -= amount * settings.STOCK_CORRECTION_MULTIPLIER
                item.save()
                try:
                    # async call would be sweet, but this is usually pretty fast
                    if settings.ENABLE_MQTT:
                        publish_stock(item)
                except:
                    pass

            self.balance -= item.price * amount
            return self.balance
        else:
            raise TypeError

    def set_pin(self, raw_pin):
        self.pin = make_password(raw_pin)

    def check_pin(self, raw_pin):
        """
        Returns a boolean of whether the raw_pin was correct. Handles
        hashing formats behind the scenes.
        Basically a copy of the check_password function from contrib.auth.models.User
        """

        def setter(raw_pin):
            """
            In case we need to update the hash and the pin was valid
            """
            self.set_pin(raw_pin)
            self.save(update_fields=["pin"])

        return check_password(raw_pin, self.pin, setter)


    def last_day(self):
        now = timezone.now()
        transactions = self.user.transactionlog_set.filter(date__year=now.year,
                                                           date__month=now.month,
                                                           date__day=now.day)
        items = transactions.exclude(item=None)
        drinks = transactions.exclude(item__drink=None)
        money_spend = items.aggregate(models.Sum('balance_change'))['balance_change__sum']
        drinks = {'transactions': drinks}
        drinks['volume'] = drinks['transactions'].aggregate(models.Sum('item__drink__volume'))[
            'item__drink__volume__sum']
        drinks['caffeine'] = drinks['transactions'].aggregate(
            caffeine=models.Sum(models.F('item__drink__volume') * models.F('item__drink__caffeine'), output_field=models.DecimalField(max_digits=5, decimal_places=2, default=0)))['caffeine']
        drinks['kcal'] = drinks['transactions'].aggregate(
            kcal=models.Sum(models.F('item__drink__volume') * models.F('item__drink__kcal'), output_field=models.DecimalField(max_digits=5, decimal_places=2, default=0)))['kcal']
        if drinks['kcal'] is not None:
            drinks['kcal'] = int(drinks['kcal'])

        return {'transactions': transactions, 'spend': money_spend, 'drinks': drinks}

    @property
    def negative_funds(self):
        return self.balance < 0

    @property
    def show_public(self):
        try:
            return self.free_access or len(self.pin) > 10
        except TypeError:
            return False


# hook to the create/save methods of auth.models.User
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Account.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.account.save()

# create API Tokens for new users
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class Item(models.Model):
    """
    Generic base class for all kinds of items
    """
    name = models.CharField(max_length=32)
    description = models.TextField(blank=True)
    image = models.ImageField(upload_to="uploads/products/", blank=True)
    ean = models.CharField(max_length=13, blank=True, null=True)  # European Article Number (EAN)
    price = models.DecimalField(max_digits=5, decimal_places=2, default=1.5)  # price in euro
    slug = models.SlugField(max_length=64, unique=True)
    stock = models.PositiveIntegerField(default=0)

    creation_date = models.DateTimeField(auto_now_add=True)
    creating_user = models.ForeignKey(User, related_name="creator", null=True, on_delete=models.SET_NULL)

    last_update = models.DateTimeField(auto_now=True)
    last_update_user = models.ForeignKey(User, related_name="updater", null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name


    def itemtype(self):
        """
        not happy about this way to determine a potential subclass
        """
        try:
            if self.drink:
                return "drink"
        except Drink.DoesNotExist:
            pass
        try:
            if self.filament:
                return "filament"
        except Filament.DoesNotExist:
            pass
        return "misc"

class Drink(Item):
    volume = models.DecimalField("Menge in l", max_digits=4, decimal_places=2)
    alcohol = models.DecimalField("Alkoholgehalt in %", max_digits=3, decimal_places=1,
                                  null=True)  # null or % of alcohol
    caffeine = models.IntegerField("Koffeingehalt in mg/l", null=True)  # caffeine in mg/l
    sugar = models.IntegerField("Zuckergehalt in g/l", default=0)
    kcal = models.IntegerField("KCal je l", default=0)

    def __str__(self):
        return "%sl %s" % (self.volume, self.name)


class TransactionLog(models.Model):
    """
    Transaction log entry
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    user_doing = models.ForeignKey(User, related_name="transactions_did", blank=True, null=True, on_delete=models.CASCADE)
    balance_change = models.DecimalField("Guthabenänderung in Euro", max_digits=5, decimal_places=2, default=0)
    item = models.ForeignKey(Item, blank=True, null=True, default=None, on_delete=models.SET_NULL)
    item_amount = models.IntegerField(blank=True, null=True, default=None)
    comment = models.CharField(max_length=255, blank=True, null=True)
    date = models.DateTimeField(auto_now_add=True)
    ip = models.GenericIPAddressField(blank=True, null=True)

# Lets extend the TML with all those filaments, because why not

class FilamentType(models.Model):
    vendor = models.CharField("Hersteller", default="colorFabb", max_length=64)
    diameter = models.DecimalField("Durchmesser", max_digits=4, decimal_places=2, default=Decimal('2.85'))
    name = models.CharField("Name", default="RandomPLA", max_length=64)
    abrasive = models.BooleanField("Abrasiv", default=False)
    bed_temp_min = models.IntegerField("Druckbett-Temperatur(min) in °C", default=50)
    bed_temp_max = models.IntegerField("Druckbett-Temperatur(max) in °C", default=60)
    print_temp_min = models.IntegerField("Drucktemperatur(min) in °C", default=195)
    print_temp_max = models.IntegerField("Drucktemperatur(max) in °C", default=220)
    print_speed_min = models.IntegerField("Druckgeschwindikgeit(min) in mm/s", default=40)
    print_speed_max = models.IntegerField("Druckgeschwindikgeit(max) in mm/s", default=100)

    def __str__(self):
        return "%s %s(%smm)" % (self.vendor, self.name, self.diameter)

    def colors_available(self):
        return self.filament_set.filter(stock__gt=0)

    def get_absolute_url(self):
        return reverse('filamenttype-detail', kwargs={'pk': self.pk})

class Filament(Item):
    filamenttype = models.ForeignKey(FilamentType, on_delete=models.CASCADE)
    # just use name for color, its required anyways
    # color = models.CharField("Farbe", default="Schwarz", max_length=32)

    def __str__(self):
        return "%s %s" % (self.filamenttype, self.name)