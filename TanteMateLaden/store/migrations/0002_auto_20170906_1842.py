# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-06 16:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='drink',
            name='kcal',
            field=models.IntegerField(null=True, verbose_name='KCal je l'),
        ),
        migrations.AddField(
            model_name='drink',
            name='sugar',
            field=models.IntegerField(null=True, verbose_name='Zuckergehalt in mg/l'),
        ),
    ]
