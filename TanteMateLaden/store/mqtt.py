import paho.mqtt.client as mqtt
import asyncio
from django.conf import settings
from time import sleep


def publish_stock(item, retain=True):
    mqttc = mqtt.Client()
    mqttc.connect(settings.MQTT_HOST, port=1883, keepalive=60, bind_address="")
    mqttc.loop_start()
    mqttc.publish(settings.MQTT_BASE_TOPIC + 'stocks/' + item.itemtype() + '/' + item.slug, payload=item.stock, qos=0, retain=retain)
    mqttc.loop_stop()
