from django.core.exceptions import PermissionDenied
from django.contrib import messages
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from django.views.generic import DetailView
from django.http import Http404
from django.db.models import Count, Avg
from django.http import HttpResponse
from django.shortcuts import render, redirect, reverse

from rest_framework import viewsets, status
from rest_framework.decorators import api_view, permission_classes, action
from rest_framework.permissions import AllowAny, DjangoModelPermissionsOrAnonReadOnly
from rest_framework.response import Response
from rest_framework.authtoken.models import Token

from TanteMateLaden.ipCheck import isSpaceIP
from .models import Account, Drink, Item, TransactionLog, FilamentType
from .forms import AccountForm, UserForm, PinChangeForm
from .serializer import AccountSerializer, DrinkSerializer, ItemSerializer, TransactionLogSerializer
from .mqtt import publish_stock


# REST API VIEWS

@permission_classes((DjangoModelPermissionsOrAnonReadOnly,))
class AccountViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows accounts to be viewed or edited.
    """
    queryset = Account.objects.all().order_by('-creation_date')
    serializer_class = AccountSerializer

    def create(self, request):
        user = User(username=request.data.get('username'))
        user.set_password(request.data.get('password'))
        user.save()
        return user.account

    @action(detail=True, methods=['post', 'get'], url_path='add/funds/(?P<amount>[0-9.]+)', permission_classes=[AllowAny])
    def AddFunds(self, request, amount, pk=None):
        """
        Add Funds to an account. Respects the no_logs of the receiving user.
        If no username provided and user authed, funds will be added to own account
        Returns the new account balance
        """
        try:
            acc = Account.objects.get(pk=int(pk))
        except ValueError:  # wasnt a account id
            user = User.objects.get(username=user)
            acc = user.account
        amount = float(amount)

        acc.addFunds(amount,
                     ip=request.META.get('HTTP_X_FORWARDED_FOR'),
                     user_doing=request.user
                     )
        acc.save()
        return Response({'balance': acc.balance})


class DrinkViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows accounts to be viewed or edited.
    """
    queryset = Drink.objects.all().order_by('-creation_date')
    serializer_class = DrinkSerializer


class ItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows accounts to be viewed or edited.
    """
    queryset = Item.objects.all().order_by('-creation_date')
    serializer_class = ItemSerializer


class TransactionLogViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows accounts to be viewed or edited.
    """
    serializer_class = TransactionLogSerializer

    def get_queryset(self):
        if self.request.user.is_staff:
            return TransactionLog.objects.all().order_by('-date')
        elif self.request.user.is_authenticated:
            return TransactionLog.objects.filter(user=self.request.user).order_by('-date')
        else:
            return TransactionLog.objects.none()


@api_view(['POST',])
@permission_classes((AllowAny,))
def BuyItemView(request, item_slug, user_id=None, item_amount=1):
    # this may throw errors if user isnt authed.
    if user_id is not None:
        user = User.objects.get(id=int(user_id))
    else:
        user = request.user
    user_doing = request.user or None
    item = Item.objects.get(slug=item_slug)
    amount = int(item_amount)
    pin = request.POST.get('pin', False)
    acc = user.account

    if user == user_doing:
        # we buy for ourselves, no further checks neeeded
        acc.buyItem(item, amount, request.META.get('HTTP_X_FORWARDED_FOR'), user_doing)
    else:
        # Account is free for all
        if acc.free_access and (isSpaceIP(request.META.get('HTTP_X_FORWARDED_FOR')) or settings.DEBUG):
            comment = "Free access, no auth needed"

        # Account uses pin auth
        elif pin and acc.pin and (isSpaceIP(request.META.get('HTTP_X_FORWARDED_FOR')) or settings.DEBUG):
            if acc.check_pin(pin):
                comment = "Legitimated by PIN"
            else:
                raise PermissionDenied("Wrong PIN")

        # User is staff
        elif user_doing.is_staff:
            comment = "Legitimated by admin privileges"
        else:
            raise PermissionDenied
        acc.buyItem(item, amount, request.META.get(
            'HTTP_X_FORWARDED_FOR'), user_doing, comment)
    acc.save()
    return Response({'balance': acc.balance, 'user': acc.user.username, 'item': item.name}, status.HTTP_202_ACCEPTED)


@api_view(['POST', 'GET', 'PUT', 'PATCH'])
@permission_classes((AllowAny,))
def CashBuyItemView(request, item_slug, item_amount=1):
    # Primary for statistics, decrement the stock count by 1
    # User is expected to put some money into the basher
    # this may throw errors if user isnt authed.
    item = Item.objects.get(slug=item_slug)
    amount = int(item_amount)
    item.stock -= amount
    item.save()
    return Response({'item': item.name}, status.HTTP_202_ACCEPTED)


def signup(request):
    """ register a new account, auth him and return to / """
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect(reverse('account-index'))
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})


def indexView(request):
    """Index View, triggers Log Truncating if user logged in"""
    if request.user.is_authenticated:
        request.user.account.truncateLogs()
    if request.META.get('HTTP_X_FORWARDED_FOR', None) not in settings.PUBLIC_TERMINAL_IPS:
        # Unlimit session length on private devices
        request.session.set_expiry(60 * 60 * 24 * 7 * 12)  # 12 Weeks
    else:
        request.session.set_expiry(60 * 5)  # 5 Minutes
    return render(request, 'store/index.html')


def accountView(request):
    if request.user.is_authenticated:
        userform = UserForm(instance=request.user, prefix="user")
        accountform = AccountForm(instance=request.user.account, prefix="acc")
        pwform = PasswordChangeForm(request.user, prefix="pw")
        pinform = PinChangeForm(prefix="pin")
        transactions = request.user.transactionlog_set.order_by('-id')
        if request.method == 'POST':
            if request.POST['operation'] == "settings":
                userform = UserForm(request.POST, instance=request.user, prefix="user")
                accountform = AccountForm(request.POST, request.FILES, instance=request.user.account, prefix="acc")
                if userform.has_changed() and userform.is_valid():
                    userform.save()
                if accountform.has_changed() and accountform.is_valid():
                    accountform.save()
                messages.success(request, 'Benutzerprofil aktualisiert')

            elif request.POST['operation'] == "pin":
                pinform = PinChangeForm(request.POST, prefix="pin")
                if pinform.has_changed() and pinform.is_valid():
                    pin = pinform.cleaned_data['pin']
                    request.user.account.set_pin(pin)
                    request.user.account.save()
                    messages.success(request, 'Neuer Pin gespeichert')

            elif request.POST['operation'] == "password":
                pwform = PasswordChangeForm(request.user, request.POST, prefix="pw")
                if pwform.is_valid():
                    pwform.save()
                    messages.success(request, 'Neues Passwort gespeichert')
            elif request.POST['operation'] == "token":
                token = request.user.auth_token
                token.delete()
                Token.objects.create(user=request.user)
                messages.success(request, 'Neuer API-Token generiert')
        # set balance read only
        accountform.fields['balance'].widget.attrs['readonly'] = True
        return render(request, 'store/account/index.html', {'userform': userform,
                                                            'accform': accountform,
                                                            'pwform': pwform,
                                                            'pinform': pinform,
                                                            'transactions': transactions})
    else:
        return redirect(reverse('index'))


def statsView(request):
    """
    Be aware: All Stats are based on Logs.
    """
    total_logs = TransactionLog.objects.count()
    total_item_logs = TransactionLog.objects.exclude(item=None).count()
    item_sales = Item.objects.annotate(num_sales=Count('transactionlog')).exclude(num_sales=0).order_by('-num_sales')
    drinks = Drink.objects.order_by('name')
    return render(request, 'store/stats.html',
                  {'trans_total': total_logs, 'total_item_sales': total_item_logs, 'items': item_sales,
                   'drinks': drinks})


class FilamentTypeDetail(DetailView):
    model = FilamentType


def pushMQTT(request):
    if settings.ENABLE_MQTT:
        items = Item.objects.all()
        for i in items:
            publish_stock(i)
        return HttpResponse(status=204)
    return Http404
