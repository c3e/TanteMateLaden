from django.contrib import admin

# Register your models here.
from .models import Account, Drink, FilamentType, Filament

admin.site.register(Account)

admin.site.register(FilamentType)

@admin.register(Filament)
class FilamentAdmin(admin.ModelAdmin):
    list_display = ("name", "filamenttype", "stock", "price")
    list_editable = ("stock", "price")
    prepopulated_fields = {"slug": ("name", "filamenttype")}
    readonly_fields = ('last_update_user', 'creating_user',)

    def save_model(self, request, obj, form, change):
        if not obj.creating_user:
            obj.creating_user = request.user
        obj.last_update_user = request.user
        obj.save()

@admin.register(Drink)
class DrinkAdmin(admin.ModelAdmin):
    list_display = ("name", "stock", "volume", "alcohol", "caffeine", "sugar", "kcal")
    list_editable = ("stock",)
    prepopulated_fields = {"slug": ("name", "volume")}

    def save_model(self, request, obj, form, change):
        if not obj.creating_user:
            obj.creating_user = request.user
        obj.last_update_user = request.user
        obj.save()
