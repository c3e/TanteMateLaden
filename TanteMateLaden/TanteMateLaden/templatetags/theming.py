from django.template import Template, Library
from django import template
from django.conf import settings

register = template.Library()


@register.inclusion_tag("themes/extension.html")
def theme_css():
    template = "themes/%s.html" % settings.THEME
    return {'template': template, }


@register.filter(name='divide')
def dividable(value, arg):
    if value % arg == 0:
        return True
    return False
