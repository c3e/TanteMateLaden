import socket
from django.conf import settings

def getIPs(hostname=settings.EXTERNAL_HOST_NAME):
    ip_list = []
    ais = socket.getaddrinfo(hostname, 0, 0, 0, 0)
    for result in ais:
        ip_list.append(result[-1][0])
    ip_list = list(set(ip_list))
    return ip_list + settings.INTERNAL_IPS

def isSpaceIP(ip, hostname=settings.EXTERNAL_HOST_NAME):
    if ip in getIPs(hostname):
        return True
    return False