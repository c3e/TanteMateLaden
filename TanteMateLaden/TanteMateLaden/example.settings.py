"""
DO NOT REPLACE SETTINGS.PY WITH THIS/YOUR CONFIGURATION
Overload it instead (see uwsgi manual or whatever you use for this)

There some setting you need to change, others you might want to change.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.0/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
# Uses the 'SECRET_KEY' environment variable by default, change to a string if you like
SECRET_KEY = os.environ['SECRET_KEY']

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# Hosts allowed to access the TML.
# See https://docs.djangoproject.com/en/3.0/ref/settings/#std:setting-ALLOWED_HOSTS
ALLOWED_HOSTS = ['localhost', '127.0.0.1', '[::1]']

# TML enables very low user authentication (like None at all or very simple PINs)
# To limit the abuse potential, only requests coming from our own network are allowed to do so.
# TML checks this by comparing the user IP with our own (external) IP (EXTERNAL_HOST_NAME)
# and IPs from our local network (INTERNAL_IPS).
EXTERNAL_HOST_NAME = "somewhere.dyndns.foo"
INTERNAL_IPS = ['127.0.0.1', '::1'] + ['192.168.178.%d' % i for i in range(256)]

SESSION_COOKIE_AGE = 60 * 60 * 24 * 7 * 4  # 4 Weeks

MAIL_FROM = 'tantemate@example.com'


WSGI_APPLICATION = 'TanteMateLaden.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Enable this if you want to publish your item stocks on a mqtt server.
ENABLE_MQTT = False
MQTT_HOST = 'mqtt.somewhere.de'
MQTT_BASE_TOPIC = 'foo/bar/tantemate/'

# We need to compensate for 'black buys' in our statistics.
# Set this to 1 if all your sales going through TML. You may need to experiment with this setting
STOCK_CORRECTION_MULTIPLIER = 5

# Enables the Filament Store for 3d Printing
INCLUDE_FILAMENTS = False

# Enables the Clubstarter Application for crowdfunding.
INCLUDE_CLUBSTARTER = True

# Change this if you want a custom theme (w/o forking) instead of the TML default
THEME = "darkly.bs4"

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators
# We operate on honor anyways, no need to enforce good passwords
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    # {
    #     'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    # },
    # {
    #     'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    # },
    # {
    #     'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    # },
]

# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'de'

TIME_ZONE = 'Europe/Berlin'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
