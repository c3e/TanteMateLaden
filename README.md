[![pipeline status](https://gitlab.com/c3e/TanteMateLaden/badges/master/pipeline.svg)](https://gitlab.com/c3e/TanteMateLaden/commits/master)
[![coverage report](https://gitlab.com/c3e/TanteMateLaden/badges/master/coverage.svg)](https://gitlab.com/c3e/TanteMateLaden/commits/master)
# TanteMateLaden
TanteMateLaden is a Mete/Donatr replacement.

This app is built with Django(3) and is currently maintained by CBluoss and fronbasal.


## Installation
### Requirements
  python(3)  
  virtualenv

### Setup
    git clone TanteMateLaden
    cd TanteMateLaden
    virtualenv venv
    source venv/bin/activate
    pip install -r requirements.txt
    cd TanteMateLaden
    ./manage.py makemigrations
    ./manage.py migrate
    ./manage.py createsuperuser
    ./manage.py loaddata store/fixtures/drinks.json
 
### Run the DEV-Server
    ./manage.py runserver
 open http://localhost:8000/
 
 ???
 
 profit
 
 ### Deployment For Production
 This is typical Django-Project, take a look at https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/uwsgi/
 if your not familiar with deploying django applications.
 
 ### Configuration
Default setting are in /TanteMateLaden/TanteMateLaden/settings.py
 Its recommended to use an additional settings file instead of changing settings.py itself.
(see https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/#configuring-the-settings-module )

#### Settings you need  (or want) to change
Please take a look at the django documentation and/or comments in setting.py
* DEBUG = False
* SECRET_KEY = "%somethingLongRandomAndSecret%"
* ALLOWED_HOSTS = ['127.0.0.1', '::1', 'localhost']
* DATABASES
* MAIL_FROM
* MQTT_HOST 
* MQTT_BASE_TOPIC 
* STOCK_CORRECTION_MULTIPLIER = 5
* THEME = "darkly.bs4"


